/*
 * Copyright (c) 2016-2017, 2020 The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/errno.h>
#include <nand.h>
#include <part.h>
#include <mmc.h>
#include <asm/arch-qca-common/smem.h>
#include <asm/arch-qca-common/qca_common.h>
#include <sdhci.h>
#ifdef CONFIG_IPQ_TINY_SPI_NOR
#include <spi.h>
#include <spi_flash.h>
#endif
#if defined(CONFIG_ART_COMPRESSED) &&	\
	(defined(CONFIG_GZIP) || defined(CONFIG_LZMA))
#ifndef CONFIG_COMPRESSED_LOAD_ADDR
#define CONFIG_COMPRESSED_LOAD_ADDR CONFIG_SYS_LOAD_ADDR
#endif
#include <mapmem.h>
#include <lzma/LzmaTools.h>
#endif
#ifdef CONFIG_QCA_MMC
#ifndef CONFIG_SDHCI_SUPPORT
extern qca_mmc mmc_host;
#else
extern  struct sdhci_host mmc_host;
#endif
#endif

uchar hex2uchar(char chstr)
{
	if(chstr >= '0' && chstr <= '9')
	{
		return (chstr - '0');
	}
	else if (chstr >= 'A' && chstr <= 'F')
	{
		return (chstr - 'A' + 10);
	}
	else if (chstr >= 'a' && chstr <= 'f')
	{
		return (chstr - 'a' + 10);
	}
	else
	{
		return 0;
	}
}

/*
 * Gets the ethernet address from environment variable and return the value
 */
int get_eth_mac_address(uchar *enetaddr, uint no_of_macs)
{
	int ret = 0;
	char * addr = NULL;
	unsigned char mac_addr[6] = {0};
	int ntotal = 0;

	unsigned long long local_long_enetaddr[no_of_macs];
	unsigned char local_enetaddr[6 * no_of_macs];

	char local_mac[18] = {0};
	size_t len = 0;
	char *pchstr = NULL;
	unsigned long long mac_long = 0;
	/* Get baseMAC from environment variable */
	addr = getenv("baseMAC");
	if (addr == NULL)
	{
		fprintf(stderr, "get basemac from env fail!\n");
		ret=-1;
		return ret;
	}
	len = strlen(addr);
	if(len < sizeof(local_mac))
		memcpy(local_mac, addr, len);
    	else
        	memcpy(local_mac, addr, sizeof(local_mac) - 1);
	/* Remove : */
	pchstr = strtok(local_mac, ":");
	while(pchstr != NULL)
	{
		 mac_addr[ntotal++] = (hex2uchar(*pchstr) << 4) | hex2uchar(*(pchstr + 1));
 		pchstr = strtok(NULL, ":");
	}
	/* Chanage unsigned char to unsigned long long */
	mac_long = 
		(unsigned long long)mac_addr[0] << 40 |
		(unsigned long long)mac_addr[1] << 32 |
		(unsigned long long)mac_addr[2] << 24 |
		(unsigned long long)mac_addr[3] << 16 |
		(unsigned long long)mac_addr[4] <<  8 |
		(unsigned long long)mac_addr[5] <<  0;
	
	for (int i = 0; i < no_of_macs; i++)
	{
		/* Offset the mac addresses */
		local_long_enetaddr[i] = mac_long + i;
		local_enetaddr[6 * i + 0] = local_long_enetaddr[i] >> 40 & 0xff;
		local_enetaddr[6 * i + 1] = local_long_enetaddr[i] >> 32 & 0xff;
		local_enetaddr[6 * i + 2] = local_long_enetaddr[i] >> 24 & 0xff;
		local_enetaddr[6 * i + 3] = local_long_enetaddr[i] >> 16 & 0xff;
		local_enetaddr[6 * i + 4] = local_long_enetaddr[i] >>  8 & 0xff;
		local_enetaddr[6 * i + 5] = local_long_enetaddr[i] >>  0 & 0xff;
	}
	memcpy(enetaddr, local_enetaddr, 6 * no_of_macs);
	return ret;
}

void set_ethmac_addr(void)
{
	int i, ret;
	uchar enetaddr[CONFIG_IPQ_NO_MACS * 6];
	uchar *mac_addr;
	char ethaddr[16] = "ethaddr";
	char mac[64];
	/* Get the MAC address from environment variable */
	ret = get_eth_mac_address(enetaddr, CONFIG_IPQ_NO_MACS);
	for (i = 0; (ret >= 0) && (i < CONFIG_IPQ_NO_MACS); i++) {
		mac_addr = &enetaddr[i * 6];
		if (!is_valid_ethaddr(mac_addr)) {
			printf("eth%d MAC Address from ART is not valid\n", i);
		} else {
			/*
			 * U-Boot uses these to patch the 'local-mac-address'
			 * dts entry for the ethernet entries, which in turn
			 * will be picked up by the HLOS driver
			 */
			snprintf(mac, sizeof(mac), "%x:%x:%x:%x:%x:%x",
					mac_addr[0], mac_addr[1],
					mac_addr[2], mac_addr[3],
					mac_addr[4], mac_addr[5]);
			setenv(ethaddr, mac);
		}
		snprintf(ethaddr, sizeof(ethaddr), "eth%daddr", (i + 1));
	}
}
