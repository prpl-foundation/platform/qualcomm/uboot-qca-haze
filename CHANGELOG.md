# Changelog

## Release 2016.01.08-prpl (unreleased)

### New features in 2016.01.08-prpl

### Bug fixes in 2016.01.08-prpl

* Updated the README.md to correct Ubuntu 22.04 build instructions for U-Boot
  on Haze, addressing outdated steps and linker errors, to align with CI build
  processes and ensure the latest release is built (PCF-1157).

### Breaking changes in 2016.01.08-prpl

* Modified the GNUmakefile to set `/tmp` as the default `SDK_HOME`, fixing the
  build issue for U-Boot on Haze by avoiding SDK installation in the source
  code directory (PCF-1157).

### Known issues in 2016.01.08-prpl

## Release 2016.01.07-prpl (released 2024-03-22)

### New features in 2016.01.07-prpl

* Updated the U-Boot binary filename convention: Development builds will now
  include a release version suffix, aligning with the pattern established for
  release builds. For example, filenames will follow the format
  `u-boot-2016.01-prpl-00001-gbda86e7b324d.elfmbn`.

### Bug fixes in 2016.01.07-prpl

* Ethernet networking is now supported for U-Boot launched from NOR flash,
  enabling the use of the standard `tftpboot` command (PCF-1126).
* The issue where U-Boot started from NOR flash did not correctly detect eMMC
  is now resolved. The error `MMC: sdhci: Node Not found, skipping
  initialization` has been fixed, ensuring full eMMC accessibility (PCF-1127).

## Release 2016.01.06-prpl (released 2024-03-16)

### New features in 2016.01.06-prpl

* Enable FAT filesystem support.
* Hack in DTS support for Haze board.
* Hack in Aquantia 113C phy support on Haze board.
* Hack in Aquantia 113C phy reset into autoboot on Haze board.
* Add support for `untar` command which makes flashing of OpenWrt sysupgrade
  images easier.
* Derive MAC addresses from environment variables instead of ART partition
  (PCF-931).

### Bug fixes in 2016.01.06-prpl

* Seems like loadb command was removed for some reason in commit dd315b2f724a
  ("ipq807x: Removed unused commands to reduce footprint"), but we still need
  it for recovery purposes, so lets add it back.
* Make it possible to boot larger initramfs images up to 256MB, previously it
  was only possible to boot 64MB images max (PCF-1048).
* Backported upstream fix commit 8d4f11c203 ("bootm: fix size arg of
  flush_cache() in bootm_load_os()") which fixes possible hang of CPU due to
  access of address beyond valid memory range.
* Release builds now have correct reproducible build timestamp.

### Known issues in 2016.01.06-prpl

* U-Boot booting from NOR flash doesn't initialize network properly, so its
  not possible to use `tftpboot` command and only kermit based loading over
  very slow serial console is available - loading of U-Boot with 650kB size
  takes about 3 minutes at 115200 baud rate (PCF-1126).

## Release 2016.01.05-prpl (released 2024-03-15)

### New features in 2016.01.05-prpl

* It's now possible to build U-Boot using single `make` command.
* U-Boot version now contains `-prpl` suffix so its clear, that its U-Boot
  with QCA/WNC/prpl related changes.
* There is `C-Kermit` based NOR recovery script and procedure available.

### Bug fixes in 2016.01.05-prpl

* Its now possible to build U-Boot using GCC version 8.

### Known issues in 2016.01.05-prpl

* U-Boot flashed into NOR flash doesn't recognize eMMC "MMC: sdhci: Node Not
  found, skipping initialization" (PCF-1127).
* Aquantia PHY is randomly unable to negotiate link speed properly and
  thus `tfpboot` command fails with `10M speed not supported` error (PCF-990).
