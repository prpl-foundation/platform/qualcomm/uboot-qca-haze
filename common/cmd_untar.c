
#include <common.h>
#include <command.h>


#define UNTAR_DEBUG 0

#define HEADER_LEN 512

#define FIELD_FILENAME_OFFSET 0
#define FIELD_FILENAME_SIZE 100

#define FIELD_FILESIZE_OFFSET 124
#define FIELD_FILESIZE_SIZE 12

#define FIELD_CHECKSUM_OFFSET 148
#define FIELD_CHECKSUM_SIZE 8

#define FIELD_TAG_OFFSET 156
#define FIELD_TAG_SIZE 1

#define TAG_NORMAL_FILE '0'

#define TAG_TYPE_NORMAL_FILE 0
#define TAG_TYPE_OTHER 1


#define STATE_HEADER 0
#define STATE_CONTENT 1

#define UNTAR_TRUE 1
#define UNTAR_FALSE 0

#define UNTAR_SUCCEED 1
#define UNTAR_FAIL 0

//prefix untar_addr_ or untar_size_
#define ENV_NAME_PREFIX_ADDR "untar_addr_"
#define ENV_NAME_PREFIX_SIZE "untar_size_"
#define ENV_NAME_PREFIX_LEN 11

static inline int read_available_data_len(char *pdata, int size, char *pdata_end)
{
    if((pdata + size) <= (pdata_end))
    {
        return size;
    }
    else if(pdata < pdata_end)
    {
        return ((int)(pdata_end-pdata));
    }
    else
    {
        return 0;
    }
}

static inline int is_data_end(char *pdata)
{
    int i;

    for(i=0; i<HEADER_LEN; i++)
    {
        if(*pdata != 0)
        {
#if UNTAR_DEBUG
            printf("%s: false\n", __func__);
#endif
            return UNTAR_FALSE;    
        }
    }

#if UNTAR_DEBUG
    printf("%s: true\n", __func__);
#endif
    return UNTAR_TRUE;
}

static int oct_to_bin(char *pdata, int size)
{
    int num = 0;
    char *ptr;
    char *pdata_end=pdata+size;

    //remove potential leading zeroes
    for(ptr=pdata; ptr < pdata_end; ptr++)
    {
        if(*ptr >= '0' && *ptr <= '7')
        {
            break;
        }
    }

    for(; ptr < pdata_end; ptr++)
    {
        if(*ptr >= '0' && *ptr <= '7')
        {
            num = (num*8 + (*ptr-'0'));
        }
        else
        {
            break;
        }
    }
    return num;
}

static int check_checksum(char *pdata)
{
    int i;
    char *ptr;
    int checksum = 0;
    int checksum_in_data = 0;

    //The checksum is calculated by taking the sum of the unsigned byte values of the header record with the eight checksum bytes taken to be ASCII spaces (decimal value 32).

    for(ptr = pdata, i=0; i<HEADER_LEN; i++, ptr++)
    {
        if(i < FIELD_CHECKSUM_OFFSET || i >= (FIELD_CHECKSUM_OFFSET+FIELD_CHECKSUM_SIZE))
        {
            checksum += *(unsigned char *)ptr;
        }
        else
        {
            checksum += 0x20;
        }
    }

    checksum_in_data = oct_to_bin(pdata+FIELD_CHECKSUM_OFFSET, FIELD_CHECKSUM_SIZE);
    if(checksum == checksum_in_data)
    {
#if UNTAR_DEBUG
        printf("%s: checksum matched 0x%8.8x\n", __func__, checksum);
#endif
        return UNTAR_TRUE;        
    }
    else
    {
#if UNTAR_DEBUG
        printf("%s: checksum unmatched data: 0x%8.8x cal: 0x%8.8x\n", __func__, checksum_in_data, checksum);
#endif
        return UNTAR_FALSE;        
    }
    
}

int untar(char *pdata, int size, char **ppfilename_list, int file_num)
{
    char filename[FIELD_FILENAME_SIZE];
    char env_name[FIELD_FILENAME_SIZE+ENV_NAME_PREFIX_LEN]; //example untar_addr_root, untar_size_root
    char *pcontent_start;
    char tag_type;
    int state = STATE_HEADER;
    char *pdata_end = pdata + size;
    char *pdata_in_process = pdata;
    int len;
    int file_size;
    int block_num;
    int i;
    char **ppfilename;
    char *pname;

#if UNTAR_DEBUG
    printf("%s: data: 0x%8.8lx data_end:0x%8.8lx\n", __func__, (unsigned long)pdata, (unsigned long)pdata_end);
    printf("%s: size: %d\n", __func__, size);
#endif    

    while(pdata_in_process < pdata_end)
    {
        switch(state)
        {
            case STATE_HEADER:
#if UNTAR_DEBUG
                printf("%s: STATE_HEADER\n", __func__);
#endif
                len = read_available_data_len(pdata_in_process, HEADER_LEN, pdata_end);
                if(len != HEADER_LEN)
                {
                    printf("Untar Error: Insufficent header data!");
                    return UNTAR_FALSE;
                }

                tag_type = *(pdata_in_process + FIELD_TAG_OFFSET);

                if(tag_type == 0)
                {
                    if(is_data_end(pdata_in_process) == UNTAR_FALSE)
                    {
                        printf("Untar Error: Incorrect tag type 0!");
                        return UNTAR_FALSE;
                    }

                    return UNTAR_TRUE;
                }

                if(check_checksum(pdata_in_process) == UNTAR_FALSE)
                {
                    printf("Untar Error: Incorrect checksum!");
                    return UNTAR_FALSE;
                }

                filename[sizeof(filename)-1] = '\0';
                memcpy(filename, pdata_in_process + FIELD_FILENAME_OFFSET, FIELD_FILENAME_SIZE);

                file_size = oct_to_bin(pdata_in_process + FIELD_FILESIZE_OFFSET, FIELD_FILESIZE_SIZE);
                pdata_in_process += HEADER_LEN;

                if(file_size > 0)
                {
                    state = STATE_CONTENT;
                    pcontent_start = pdata_in_process;
                }
                
#if UNTAR_DEBUG
                printf("%s: filename: %s\n", __func__, filename);
                printf("%s: file size: 0x%\n", __func__, file_size);
                if(file_size > 0)
                {
                    printf("%s: content start: 0x%8.8lx\n", __func__, (unsigned long)pcontent_start);
                }
                printf("%s: tag: %c\n", __func__, tag_type);
#endif
                break;
            case STATE_CONTENT:
#if UNTAR_DEBUG
                printf("%s: STATE_CONTENT\n", __func__);
#endif
                len = read_available_data_len(pdata_in_process, file_size, pdata_end);
                if(len != file_size)
                {
                    printf("Untar Error: Insufficent file data!");
                    return UNTAR_FALSE;
                }

                if(tag_type == TAG_NORMAL_FILE)
                {
                    printf("filename: %s\n", filename);
                    printf("file addr: 0x%lx\n", (unsigned long)pcontent_start);
                    printf("file size: 0x%x\n", file_size);

                    if(ppfilename_list != NULL)
                    {
                        for(i=0, ppfilename = ppfilename_list; i<file_num; i++, ppfilename++)
                        {
                            //we only compare the filename and do not check path name.
                            pname = strstr(filename, *ppfilename);
                            if(pname != NULL)
                            {
                                if((pname == filename || *(pname-1) == '/') && strcmp(pname, *ppfilename) == 0)
                                {
                                    snprintf(env_name, sizeof(env_name),"%s%s", ENV_NAME_PREFIX_ADDR, *ppfilename);
                                    setenv_hex(env_name, (ulong)pcontent_start);
                                    snprintf(env_name, sizeof(env_name),"%s%s", ENV_NAME_PREFIX_SIZE, *ppfilename);
                                    setenv_hex(env_name, file_size);
                                }
                            }
                        }
                    }
                }

                block_num = file_size / HEADER_LEN;
                pdata_in_process += (block_num * HEADER_LEN);
                if((file_size % HEADER_LEN) != 0)
                {
                    pdata_in_process += HEADER_LEN;
                }

                state = STATE_HEADER;
                break;
            default:
                printf("Untar Error: Unknown State!");
                return UNTAR_FALSE;
        }
    }
    printf("Untar Error: Run out of data!");
    return UNTAR_FALSE;
}

static int do_untar(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{   
    unsigned long src;
    int size;
    int rv = UNTAR_FALSE;
    int i;    
    char env_name[FIELD_FILENAME_SIZE+ENV_NAME_PREFIX_LEN]; //example untar_addr_root, untar_size_root
    char **ppfilename = NULL;

    if(argc >= 3)
    {
    	src = simple_strtoul(argv[1], NULL, 0);
    	size = simple_strtoul(argv[2], NULL, 0);
        if(argc > 3)
        {
            //delete the env name which kept the untar result before.
            for(i=3; i<argc; i++)
            {
                snprintf(env_name, sizeof(env_name), "%s%s", ENV_NAME_PREFIX_ADDR, argv[i]);
                setenv(env_name, NULL);
                snprintf(env_name, sizeof(env_name), "%s%s", ENV_NAME_PREFIX_SIZE, argv[i]);
                setenv(env_name, NULL);
            }

            ppfilename = (char **)&(argv[3]);
        }

        rv = untar((char *)src, size, ppfilename, argc-3);
    }        
    else
    {
	    return CMD_RET_USAGE;
	}

    if(rv == UNTAR_TRUE)
    {
	    return CMD_RET_SUCCESS;
    }
    else
    {
	    return CMD_RET_FAILURE;
    }
}

U_BOOT_CMD(
	untar,	7,	1,	do_untar,
	"get the file info in tarball-formatted memory",
	"srcaddr srcsize [file 1] [file 2] [file 3] [file 1]"
);
