# U-Boot for the prpl Foundation Haze platform

Goal of this project is to provide source code and binary releases for the
U-Boot bootloader as being shipped with the reference platform devices or
otherwise distributed within prpl Foundation.

Latest release is [2016.01.07-prpl](https://gitlab.com/prpl-foundation/platform/qualcomm/uboot-qca-haze/-/releases/2016.01.07-prpl)
released 2024-03-22.

## Building

### Using Ubuntu 22.04 build host

#### Installing build dependencies

```sh
sudo apt-get update
sudo apt-get install --yes bc gcc git gpg make python2 wget xz-utils
```

#### Building U-Boot

The goal of this step is to produce `u-boot.elfmbn` bootloader binary which
can be flashed from within U-Boot itself.

```sh
git clone https://gitlab.com/prpl-foundation/platform/qualcomm/uboot-qca-haze
cd uboot-qca-haze
git checkout 2016.01.07-prpl
make PYTHON=python2
```

*Note: the build is limited to a single make job intentionally for reproducibility.*

## Installation

### eMMC flash layout

Haze board comes preloaded from factory with the following EFI partition
layout on the eMMC flash memory.

| Part# | Start LBA | End LBA |  Size   |    Name     | Description |
| ----- | --------- | ------- | ------- | ----------- | ----------- |
|  14   |   0x3022  |  0x3221 | 0x40000 | APPSBLENV   | U-Boot environment |
|  15   |   0x3222  |  0x3821 | 0x600   | APPSBL      | U-Boot |

### Flashing using TFTP method

You can flash `u-boot.elfmbn` U-Boot binary from the U-Boot using following commands.

```sh
tftpboot 0x44000000 u-boot.elfmbn
mmc erase 0x3222 0x600
mmc write 0x44000000 0x3222 0x600
reset
```

### Recovery

#### NOR based recovery procedure

If you manage to render your device unusable during the flashing procedure,
you can try to recover using NOR based recovery procedure.

This recovery procedure changes boot source medium from eMMC to NOR flash. For
some reason the factory provisioned U-Boot doesn't initialize properly the
network interfaces so its not possible to `tftpboot` the U-Boot image and thus
only working procedure is to recover using `kermit` protocol over serial
console.

##### Using Ubuntu 22.04 host

###### Installing host recovery dependencies

Install `C-Kermit` package which is a combined serial and network
communication software package offering automation of communication tasks.

```sh
sudo apt-get update
sudo apt-get install --yes ckermit
```

###### Switching boot source medium

You can change boot source medium from `eMMC` to `NOR` flash by a [slide
switch](https://duckduckgo.com/?q=slide+switch&iax=images) marked `SW1` next
to the `UART` based serial console connector (2.54" jumper header), default
position of the slide switch is next to the `UART` header and sets booting
from eMMC and you need to move it to the opposite direction and thus change
boot source to `NOR` flash.

| U-Boot log message       | Boot flash  |
| ------------------------ | ----------- |
| MMC:   \<NULL\>: 0 (eMMC)|     eMMC    |
| MMC:   \<NULL\>: 0       |     NOR     |

###### Changing boot source to NOR flash

1. Remove power supply from the board
1. Move slide of `SW1` switch to the position
1. Power on the board

In the serial console log output, you should now see `MMC:   <NULL>: 0`
message which means, that the boot source is NOR flash memory.

###### Recover board using C-Kermit script

There is `C-Kermit` based [kermit-recovery](scripts/prpl/kermit-recovery)
script available, which should help you automate the recovery procedure. Just
pass it your serial console `/dev/ttyUSB0` device and path to `u-boot.elfmbn`
U-Boot image.

```sh
./scripts/prpl/kermit-recovery /dev/ttyUSB0 u-boot.elfmbn
```

##### Using Windows host

You can follow [recovery procedure](https://docs.google.com/document/d/1RuwLlz-Omt9_M2HTCvgtfETdTAKNl7yD)
(needs prpl Foundation membership) for Windows using
[TeraTerm](https://teratermproject.github.io/index-en.html) terminal emulator.

## Contributing

Follow the [contribution guidelines](CONTRIBUTING.md).

## Releases

Latest release is [2016.01.07-prpl](https://gitlab.com/prpl-foundation/platform/qualcomm/uboot-qca-haze/-/releases/2016.01.07-prpl)
released 2024-03-22.

You can find past releases on the [release page](https://gitlab.com/prpl-foundation/platform/qualcomm/uboot-qca-haze/-/releases).
Changelog for the releases is available in [CHANGELOG.md](CHANGELOG.md).

### Versioning scheme

Currently the plan is to follow upstream based versioning scheme in the
form of `2016.01.yy-prpl` where:

* `2016.01` is the upstream release used as a base
* `yy` is two digit number used to mark `prpl` releases
* `-prpl` is fixed text to make it clear, that its `prpl` related U-Boot

### Creating releases

Lets say, that we're currently working on `2016.01.05-prpl` release, then we
can actually make that release by issuing following command:

```sh
./scripts/prpl/prepare-release.sh 5
```

which will create release notes and `2016.01.05-prpl` signed release Git tag
including following branches:

* prpl/make-release-2016.01.05-prpl
* prpl/mark-next-release-2016.01.06-prpl

which can then be used in the following manner:

1. Push prepared release `git push origin prpl/make-release-2016.01.05-prpl`.
1. Create a merge request, flash U-Boot and check if everything works as
   expected.
1. Merge the prepared release.
1. Push the tag with `git push origin 2016.01.05-prpl` and let CI prepare
   release.
1. Push preparation for next release `git push origin prpl/mark-next-release-2016.01.06-prpl`.
1. Once verified, merge it so the work on next `2016.01.06-prpl` release can start.

## Branches

| branch | description |
| ------ | ----------- |
| `prpl/2016.01` | Development and release branch based on Qualcomm's [u-boot-2016](https://git.codelinaro.org/clo/qsdk/oss/boot/u-boot-2016)|

## Repository content origin

Origin of this repository was created using following commands:

```sh
git clone https://git.codelinaro.org/clo/qsdk/oss/boot/u-boot-2016 && cd u-boot-2016
git reset --hard 97a07285b2d69a235073660c5bb7be7c8dd27ef8
```

Then we've applied fixes and various WNC patches delivered for Haze board support.
