SDK_HOME?=/tmp
SDK_VERSION:=openwrt-sdk-21.02.7-ipq806x-generic_gcc-8.4.0_musl_eabi.Linux-x86_64
SDK_FILE:=$(SDK_VERSION).tar.xz
SDK_URL:=https://archive.openwrt.org/releases/21.02.7/targets/ipq806x/generic
SDK_DIR:=$(SDK_HOME)/$(SDK_VERSION)
TOPDIR:=$(SDK_DIR)
DLDIR:=$(SDK_HOME)/dl
STAGING_DIR=$(SDK_DIR)/staging_dir
TOOLCHAIN_DIR=$(STAGING_DIR)/toolchain-arm_cortex-a15+neon-vfpv4_gcc-8.4.0_musl_eabi
DTC:=$(SDK_DIR)/build_dir/target-arm_cortex-a15+neon-vfpv4_musl_eabi/linux-ipq806x_generic/linux-5.4.238/scripts/dtc/dtc
GPG_KEY_SERVER:=hkps://keyserver.ubuntu.com:443
PYTHON?=python
VERBOSE?=

export PATH:=$(TOOLCHAIN_DIR)/bin:$(PATH)
export STAGING_DIR:=$(STAGING_DIR)

u-boot.elfmbn: $(SDK_DIR)/COPYING
	make -j1 V=$(VERBOSE) -f Makefile \
		HOSTSTRIP=true \
		CROSS_COMPILE=arm-openwrt-linux-muslgnueabi- \
		TARGETCC=arm-openwrt-linux-muslgnueabi-gcc \
		KCPPFLAGS="" \
		HOSTLDFLAGS="-znow -zrelro -pie" \
		LDFLAGS="" \
		DTC="$(DTC)" \
		PYTHON="$(PYTHON)" \
		SOURCE_DATE_EPOCH=$(shell $(SDK_DIR)/scripts/get_source_date_epoch.sh) \
		clean ipq807x_defconfig all
	@echo
	@suffix=$$(make -s ubootrelease -f Makefile) && \
		cp u-boot u-boot-$${suffix} && \
		cp u-boot.elfmbn u-boot-$${suffix}.elfmbn && \
		sha256sum u-boot-$${suffix} u-boot-$${suffix}.elfmbn | tee sha256sums

$(DLDIR)/.created:
	mkdir -p $(DLDIR)
	@touch $@

$(DLDIR)/sha256sums.asc: $(DLDIR)/.created
	wget $(SDK_URL)/$$(basename $@) -O $@
	@touch $@

$(DLDIR)/sha256sums: $(DLDIR)/.created
	wget $(SDK_URL)/$$(basename $@) -O $@
	@touch $@

$(DLDIR)/$(SDK_FILE): $(DLDIR)/.created
	wget $(SDK_URL)/$$(basename $@) -O $@
	@touch $@

$(DLDIR)/.downloaded: $(DLDIR)/sha256sums.asc $(DLDIR)/sha256sums $(DLDIR)/$(SDK_FILE)
	@touch $@

$(DLDIR)/.gpgkeys: $(DLDIR)/.downloaded
	gpg --keyserver $(GPG_KEY_SERVER) --receive-keys 0x88CA59E88F681580
	@touch $@

$(DLDIR)/.verified: $(DLDIR)/.gpgkeys
	gpg --with-fingerprint --verify $(DLDIR)/sha256sums.asc
	cd $(DLDIR) && sha256sum --check --ignore-missing sha256sums
	@touch $@

$(SDK_DIR)/COPYING: $(DLDIR)/.verified
	tar --xz -xf $(DLDIR)/$(SDK_FILE) -C $(SDK_HOME)
	@touch $@
